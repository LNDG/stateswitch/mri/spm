#!/bin/bash

# Compile SPM12 with the variability toolbox added in

module load matlab/R2016b
matlab

restoredefaultpath
addpath('/home/beegfs/kosciessa/StateSwitch/WIP/G_GLM/T_tools/spm12')
addpath('/home/beegfs/kosciessa/StateSwitch/WIP/G_GLM/T_tools/spm12/matlabbatch')
addpath('/home/beegfs/kosciessa/StateSwitch/WIP/G_GLM/T_tools/spm12/config')
spm_make_standalone()