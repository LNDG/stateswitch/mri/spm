#!/bin/bash

RootPath=/home/beegfs/kosciessa/StateSwitch/WIP/G_GLM
DATADIR=${RootPath}/B_data/D_batch1stLevel_v2

#subjList="1117 1118 1120 1124 1125 1126 1131 1132 1135 1136 1151 1160 1164 1167 1169 1172 1173 1178 1182 1214 1215 1216 1219 1223 1227 1228 1233 1234 1237 1239 1240 1243 1245 1247 1250 1252 1257 1261 1265 1266 1268 1270 1276 1281"
#subjList="1117"
subjList="1215"

for subj in $subjList; do
	echo "#PBS -N STSWD_SPM_${subj}" 	> job
	echo "#PBS -l nodes=1:ppn=1"				>> job
	echo "#PBS -l mem=16gb" 						>> job
	echo "#PBS -l walltime=30:00:00" 			>> job
	echo "#PBS -j oe" 							>> job
	echo "#PBS -o ${RootPath}/Y_logs/"			>> job
	echo "#PBS -m n"							>> job
	echo "#PBS -d ." 								>> job
	echo "module load spm12  ; run_spm12.sh /mcr run ${DATADIR}/${subj}_SPM1stBatchGLM.mat" 	>> job
	qsub job
	rm job
done
