% Specify design model at first level
% use concatenation across runs and correct for concatenation in design matrix

% N = 44 YA
IDs = {'1117';'1118';'1120';'1124';'1125';'1126';'1131';'1132';'1135';'1136';...
    '1151';'1160';'1164';'1167';'1169';'1172';'1173';'1178';'1182';'1214';'1215';...
    '1216';'1219';'1223';'1227';'1228';'1233';'1234';'1237';'1239';'1240';'1243';...
    '1245';'1247';'1250';'1252';'1257';'1261';'1265';'1266';'1268';'1270';'1276';'1281'};

pn.root = '/Users/kosciessa/Desktop/beegfs/StateSwitch/WIP/G_GLM/';
addpath([pn.root, 'T_tools/spm12/']); % add spm functions

for indID = 1:numel(IDs)
    % initialize SPM batch processing
    spm('defaults','fmri')
    spm_jobman('initcfg');
    % specify SPM job
    load([pn.root, 'B_data/D_batch1stLevel_v2/',IDs{indID},'_SPM1stBatchGLM.mat'], 'matlabbatch');
%     % run job
%     spm_jobman('run', matlabbatch);
    % correct matrix for independent runs
    % https://en.wikibooks.org/wiki/SPM/Concatenation
    spm_fmri_concatenate([matlabbatch{1}.spm.stats.fmri_spec.dir{1},IDs{indID},'_SPM1stBatchGLM.mat'], [1054, 1054, 1054, 1054]);
    clear matlabbatch N;
end
