function C_performLSS(ID)

    pn.root = '/home/beegfs/kosciessa/StateSwitch/WIP/G_GLM/';

    %addpath([pn.root, 'T_tools/misc-fmri-code-master/lss/'])

    spmFolder = [pn.root, 'B_data/F_GLM1stLevelout_v4_ST/'];
    spmSubFolder = '/';

    outFolder = [pn.root, 'B_data/F_GLM1stLevelout_v4_ST/'];
    outSubFolder = '/lss/';

    % LSS settings
    includeConditions = {'STIM'};
    settings.model = 2;            % 1- Rissman, 2- LSS
    settings.useTempFS = 0;        % 0- do not use temporary files, 1- use temporary files
    settings.overwrite = 0;        % 0- do not overwrite, 1- overwrite

    spmDir = [spmFolder, ID, spmSubFolder];
    outDir = [outFolder, ID, outSubFolder];
    images = lssGenerateBetasSpm(ID, spmDir, outDir, includeConditions, settings);
    save([outDir, 'images.mat']);
    