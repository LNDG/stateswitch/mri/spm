#!/bin/bash

# This script prepares tardis by compiling the necessary function in MATLAB.

ssh tardis # access tardis

# check and choose matlab version
module avail matlab
module load matlab/R2016b

# compile functions

matlab
%% go to analysis directory containing .m-file
cd('/home/beegfs/kosciessa/StateSwitch/WIP/G_GLM/A_scripts/D_GLM_LSS_stim/C_performLSS/')
%% compile function and append dependencies
mcc -m C_performLSS.m -a /home/beegfs/kosciessa/StateSwitch/WIP/G_GLM/T_tools/misc-fmri-code-master/lss